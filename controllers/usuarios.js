var Usuarios = require('../models/usuarios');

exports.list = function (req, res, next) {
    Usuarios.find({}, (err, usuariosResponse) => {
        res.render('usuarios/index', {
            usuarios: usuariosResponse
        })
    });
};

exports.create_get = function (req, res, next) {
    res.render('usuarios/create', {
        errors: {},
        usuario: new Usuarios
    })
}

exports.create = function (req, res) {
    if (req.body.pwd !== req.body.confirm_pwd) {
        res.render('usuarios/create', {
            errors: {
                confirm_pwd: {
                    message: 'No coinciden la contraseña'
                }
            }
        });
        return;
    }
    createUser = {
        nombre: req.body.nombre,
        apellido: '',
        mail: req.body.mail,
        password: req.body.pwd

    };
    console.log('se va a crear', createUser);
    Usuarios.create(createUser, (err, nuevoUsuario) => {
        if (err) {
            console.log('error', err.message);
            res.render('/usuarios/create', {
                errors: err.message,
                usuario: new Usuarios({
                    nombre: req.body.nombre,
                    apellido: '',
                    mail: req.body.mail,
                    password: req.body.pwd
                })
            });
        } else {
            nuevoUsuario.enviar_email_bienvenida();
            console.log('Redireccionar', nuevoUsuario)
            res.redirect('../../usuarios');
        }
    });


};

exports.update_get = function (req, res) {
    Usuarios.findById(req.params.id, (err, usuarioResponse) => {
        if (err) {
            console.log('Error en la busqueda para actualizar en estado 25')
        }
        console.log('La busqueda encontro', usuarioResponse);
        res.render('usuarios/update', {
            errors: {},
            usuario: usuarioResponse
        })
    });

};

exports.update = function (req, res) {
    console.log('ingreso a actualizar')
    var userUpdate = {
        nombre: req.body.nombre,
        apellido: req.body.apellido,
        mail: req.body.mail
    };
    console.log('ingreso a actualizar', userUpdate);

    Usuarios.findOneAndUpdate(req.params.id, userUpdate, function (err, usuarios) {
        if (err) {
            console.log('Ocurrio un error en la actualizacion linea 42')
            res.render('usuarios/update', {
                errors: {
                    err
                },
                usuario: new Usuarios({
                    nombre: req.body.nombre,
                    apellido: req.body.apellido,
                })
            });
        } else {
            console.log('Redireccionar')
            res.redirect('../../usuarios');
        }
    })
};

exports.delete = function (req, res, next) {
    Usuarios.findByIdAndDelete(req.body.id, function (err) {
        if (err) {
            console.log('Error en la eliminacion');
            next(err);
        } else {
            console.log('Redireccionar')
            res.redirect('../../usuarios');
        }
    });
}