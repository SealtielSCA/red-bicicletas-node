var mailConfigSend = require('../../mailer/mailer');

describe('Enviar mail', () => {
    it('Vamos a enviar un correo', (done) => {
        let message = {
            from: 'no-reply@redbicicletas.com',
            to: 'lemuel.mayert72@ethereal.email',
            subject: 'Nodemailer is unicode friendly ✔',
            text: 'Hello to myself!',
            html: '<p><b>Hello</b> to myself!</p>'
        };

        mailConfigSend.sendMail(message, function (error) {
            if (error) {
                console.log('Error en el envio', error)
            }
            console.log('correo enviado correctamentamente');
            done();
        });

    });
})