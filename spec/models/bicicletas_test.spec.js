var mongoose = require('mongoose');
var Bicicletas = require('../../models/bicicleta')

//testing
describe('Testing Bicicletas', () => {
    //conexion en cada prueba y se ejeucta antes de cada prueba
    beforeEach(function (done) {
        var mongoDB = 'mongodb://localhost/testdbbiciletas';
        mongoose.connect(mongoDB, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });

        var db = mongoose.connection;
        db.on('error', console.error.bind(console, 'mongoDB conection error'));
        db.once('open', () => {
            console.log('MongoDB are conected to test database')
            done();
        });
    });

    //se ejecuta despues de cada prueba
    afterEach(function (done) {
        Bicicletas.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            done();
        });
    });
    //primer test
    describe('Biciletas.createInstance', () => {
        it('crea una instancia de la Bicicleta', () => {
            var bici = Bicicletas.createInstance(1, 'rojo', 'urbana', [4.6401611, -74.147931]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe('rojo');
            expect(bici.modelo).toBe('urbana');
            expect(bici.ubicacion[0]).toBe(4.6401611);
            expect(bici.ubicacion[1]).toBe(-74.147931);
        });
    })
    //buscar todo
    describe('Biciletas.allBicis', () => {
        it('comienza vacia', () => {
            Bicicletas.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);
            });
        })
    });
    
    //agregar
    describe('Biciletas.add', () => {
        it('agrega una bici', (done) => {
            var aBici = Bicicletas.createInstance(1, 'rojo', 'urbana', [4.6401611, -74.147931]);
            Bicicletas.add(aBici, function (err, bicis) {
                if (err) console.log(err);
                Bicicletas.allBicis(function (err, bicis) {
                    expect(bicis.length).toBe(1);
                    expect(bicis[0].code).toBe(aBici.code);

                    done()
                });
            });
        })
    });
    //busqueda
    describe('Biciletas.FindBycode', () => {
        it('Debe regresar la bicicleta con codigo 1 ', (done) => {
            Bicicletas.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);

                var aBici = Bicicletas({
                    code: 1,
                    color: "verde",
                    modelo: "urbana"
                });
                Bicicletas.add(aBici, function (err, nuevaBici) {
                    if (err) console.log('error add', err);

                    var aBici2 = Bicicletas({
                        code: 2,
                        color: "verde",
                        modelo: "urbana"
                    });
                    Bicicletas.add(aBici2, function (err, nuevaBici) {
                        if (err) console.log('error add 2', err);
                        Bicicletas.findByCode(aBici.code, function (error, targetBici) {
                            if (err) console.log('error findbyid', err);
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);

                            done();
                        });
                    });
                });
            });

        })
    });
    //remove object
    describe('Biciletas.removeByCode', () => {
        it('Se elimina el codigo 1 ', (done) => {
            Bicicletas.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);

                var aBici = Bicicletas({
                    code: 1,
                    color: "verde",
                    modelo: "urbana"
                });
                Bicicletas.add(aBici, function (err, nuevaBici) {
                    if (err) console.log('error add', err);

                    var aBici2 = Bicicletas({
                        code: 2,
                        color: "verde",
                        modelo: "urbana"
                    });
                    Bicicletas.add(aBici2, function (err, nuevaBici) {
                        if (err) console.log('error add 2', err);

                        Bicicletas.removeByCode(aBici.code, function (error, targetBici) {
                            expect(targetBici.ok).toBe(1);
                            Bicicletas.allBicis(function (err, bicis) {
                                expect(bicis.length).toBe(1);
                                done();
                            });
                        });
                    });
                });
            });
        })
    });
});