var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var Usuario = require('../../models/usuarios');
var Reserva = require('../../models/reservas');


describe('Testing of Usuario', () => {
    //conexion en cada prueba y se ejeucta antes de cada prueba
    beforeEach(function (done) {
        var mongoDB = 'mongodb://localhost/testdbbiciletas';
        mongoose.connect(mongoDB, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });

        var db = mongoose.connection;
        db.on('error', console.error.bind(console, 'mongoDB conection error'));
        db.once('open', () => {
            console.log('MongoDB are conected to test database')
            done();
        });
    });

    //se ejecuta despues de cada prueba
    afterEach(function (done) {
        Reserva.deleteMany({}, function (err, success) {
            if (err) console.log('reserva', err);
            Usuario.deleteMany({}, function (err, success) {
                if (err) console.log('usuario', err);
                Bicicleta.deleteMany({}, function (err, success) {
                    if (err) consolelog('bicicleta', erro);
                    done();
                });
            });
        });
    });

    //test 
    describe('Creando un usuario que reserva una bici', () => {
        it('debe existir una reserva', (done) => {

            const usuario = new Usuario({
                nombre: 'martinez',
                apellido: 'Perez'
            });
            usuario.save();
            const bicicleta = new Bicicleta({
                code: 2,
                color: "verde",
                modelo: "urbana"
            });
            bicicleta.save();

            var hoy = new Date();
            var mañana = new Date();
            mañana.setDate(hoy.getDate() + 1);

            usuario.reservar(bicicleta.id, hoy, mañana, function (err, reserva) {
                //console.log('estoy aqui1'+reserva);
                //console.log(Reserva.find({}).populate('bicicletas').exec(function(err, reservas){ console.log('estoy aqui 2 ',reservas)}));
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function (err, reservas) {
                    //console.log('estoy aqui 1' + reservas[0].bicicleta); //tener cuidado con esta 
                    //console.log('estoy aqui 2' + reservas[0].usuario);
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    expect(reservas[0].bicicleta.code).toBe(2);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                });
            });

        });
    });

})