var express = require('express');
var router = express.Router();
var bicicletasController = require('../controllers/bicicletas'); //obtener controller

/* GET page bicicletas. */
router.get('/', bicicletasController.bicicleta_list);
router.get('/create', bicicletasController.bicicleta_create_get);
router.post('/create', bicicletasController.bicicleta_create_post);

//update
router.get('/:id/update', bicicletasController.bicicleta_update_get);
router.post('/:id/update', bicicletasController.bicicleta_update_post);
//delete
router.post('/:id/delete', bicicletasController.bicicleta_delete_post);





module.exports = router;