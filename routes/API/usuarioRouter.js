var express = require('express');
var router = express.Router();
var usuarioController = require('../../controllers/API/usuarioControllerAPI');


router.get('/', usuarioController.usuarios_list);
router.post('/create', usuarioController.usuarios_create);
router.post('/update', usuarioController.usuarios_create);//TODO
router.post('/reserva', usuarioController.usuario_reserva);


module.exports = router;  