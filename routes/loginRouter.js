var express = require('express');
var router = express.Router();
var loginController = require('./../controllers/login');

router.get('/', loginController.login_get);

module.exports = router;