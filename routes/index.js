var express = require('express');
var router = express.Router();

var tokenClient;
/* GET home page. */
router.get('/out', function (req, res, next) {
  tokenClient=null
  res.redirect('/');
});


router.get('/', function (req, res, next) {
  if (tokenClient != undefined) {
    res.redirect('/any/' + tokenClient);
  } else {  
  res.render('index', { title: 'Express'});
}
});

router.get('/any/:token', function (req, res, next) {
  console.log('tengo el token', req.params.token)
  if (req.params.token != undefined) {
    tokenClient = req.params.token;
  }
  res.render('index', {
    title: 'Express'
  });
});


module.exports = router;