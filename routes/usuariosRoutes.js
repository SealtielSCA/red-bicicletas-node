var express = require('express');
var router = express.Router();
var usuariosContoller = require('../controllers/usuarios')

router.get('/', usuariosContoller.list); //obtener lista
router.get('/create', usuariosContoller.create_get); //obtener pantalla de creacion
router.post('/create', usuariosContoller.create); //crear usuarios
//update
router.get('/:id/update', usuariosContoller.update_get); //obtener pantallas
router.post('/:id/update', usuariosContoller.update);//actualizar
//delete
router.post('/:id/delete', usuariosContoller.delete);//actualizar

module.exports = router;