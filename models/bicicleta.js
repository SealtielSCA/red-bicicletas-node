var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number],
        index: { //crear indice por tipo geografico llamado 2dshere
            type: '2dsphere',
            sparce: true
        }
    }
});

//crear instancia de mongo 
bicicletaSchema.statics.createInstance = function (code, color, modelo, ubicacion) {
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });

};

//modelo prototipo
bicicletaSchema.methods.toString = function () {
    return 'code:' + this.code + " | color: " + this.color+"| id: "+this.id;
};
//obtener todos las bicicletas
bicicletaSchema.statics.allBicis = function (cb) {    
    return this.find({}, cb)
};
//crear objeto
bicicletaSchema.statics.add = function (aBici, cb) {
    console.log('ingreso a crear -->' + aBici)
    this.create(aBici, cb);
}
//busqueda
bicicletaSchema.statics.findByCode = function (aCode, cb) {
    return this.findOne({
        code: aCode
    }, cb);
};
//remove
bicicletaSchema.statics.removeByCode = function (aCode, cb) {
    return this.deleteOne({
        code: aCode
    }, cb);
};




module.exports = mongoose.model('Bicicleta', bicicletaSchema);