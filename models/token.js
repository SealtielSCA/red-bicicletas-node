var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//permite validar el usuario es nuevo 
var TokenSchema = new Schema({
    usuario_Id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Usuarios',
        required: true
    },
    token: {
        type: String,
        required: true
    },
    createAtoken: {
        type: Date,
        required: true,
        default: Date.now,
        expires: 43200 // el documento se elimina cuando esto expire en mongoDB
    }
});

module.exports = mongoose.model('Token', TokenSchema);