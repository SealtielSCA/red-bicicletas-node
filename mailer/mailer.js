var nodemailer = require('nodemailer');
var sgtransport = require('nodemailer-sendgrid-transport');

let mailConfig
if (process.env.NODE_ENV == 'production') {
    console.log('#################################')
    console.log('Ambiente de production')

    const options = {
        auth: {
            api_key: process.env.SEND_GRID_API_SECRET
        }
    }
    mailConfig = sgtransport(options);
} else if (process.env.NODE_ENV == 'staging') {
    console.log('#################################')
    console.log('Ambiente de pruebas')
    const options = {
        auth: {
            api_key: process.env.SEND_GRID_API_SECRET
        }
    }
    mailConfig = sgtransport(options);
} else {
    mailConfig = {
        host: 'smtp.ethereal.email',
        port: 587,
        auth: {
            user: process.env.ethereal_user,
            pass: process.env.ethereal_pass
        }
    }
}

module.exports = nodemailer.createTransport(mailConfig)